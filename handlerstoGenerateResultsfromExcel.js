const fetch = require('node-fetch');
const habitData= require("./habits.js");
const coursesList= require("./courseRankings.js");
const dataList= require("./data.js");

class Handlers {
    constructor (typeform_api_url, default_form_id) {
        this.TYPEFORM_API_URL = typeform_api_url;
        this.DEFAULT_FORM_ID = default_form_id;

        this.indexHandler = this.indexHandler.bind(this);
        this.displayResultsHandler = this.displayResultsHandler.bind(this);
    }

    indexHandler(req, res) {
        if (!req.isAuthenticated()) {
            return res.end(`
            <body>
                <h3>Hello stranger!</h3>
                <p>You're not authenticated, you need to <a href="/login">authenticate via Typeform</a>.
            </body>
            `)
        }

        if (this.DEFAULT_FORM_ID) {
			console.log("formId",this.DEFAULT_FORM_ID);
            return res.redirect(`/results/${this.DEFAULT_FORM_ID}`);
        }

        let data = JSON.stringify(req.user);
        return res.end(`
        <body>
            <h3>Hello, %username%!</h3>
            <p>Here's your token:</p><p style="color: blue;">${data}</p>
            <p>Maybe you want to <a href="/logout">log out</a>?</p>
        </body>
        `);
    }

    displayResultsHandler(req, res) {
        fetch(`${this.TYPEFORM_API_URL}/forms/${req.params.id}/responses`, {
            headers: {
                Authorization: `Bearer CQ7sQVz3qj38bN3wdeksGxCxJe8VJViZWYzCtM6d82FD`,
            }
        })
        .then(res => res.json())
        .then(d => {
			let secondCourse="";
			let firstCourse= "";
			let thirdCourse= "";
			let scores=[];
			let i=0;
			var str=[];
			let recommArr=[];
			
			//console.log("d",d);
			if(d.items){
            d.items.forEach(item => {
				if(i==0){
					if(item.hidden){
						scores.push(item.hidden.score);
						scores.push(item.hidden.score2);
						scores.push(item.hidden.score3);
						scores.push(item.hidden.score4);
						
						let firstArray = getRecommendationCourses(normalizeScore(item.hidden.score,6),"Think");
						let secondArray= getRecommendationCourses(normalizeScore(item.hidden.score2,9),"Communicate"); 
						let thirdArray=  getRecommendationCourses(normalizeScore(item.hidden.score3,8),"Grow"); 
						let fourthArray= getRecommendationCourses(normalizeScore(item.hidden.score4,8),"Solve");
						
						for(let i=0;i<dataList.length;i++){
							let firstArray = getRecommendationCourses(normalizeScore(dataList[i]["CRITICAL THINKING Score  (Out of 6)"],6),"Think");
							let secondArray= getRecommendationCourses(normalizeScore(dataList[i]["READING DEEPLY Score  (Out of 9)"],6),"Communicate"); 
							let thirdArray=  getRecommendationCourses(normalizeScore(dataList[i]["LEADING SELF Score (Out of 8)"],6),"Grow"); 
							let fourthArray= getRecommendationCourses(normalizeScore(dataList[i]["STRUCTURING PROBLEMS Score (Out of 8)"],6),"Solve");
							let str=[];
							str= str.concat(firstArray,secondArray,thirdArray,fourthArray);
							str= getAppropriateCourses(str);
							recommArr.push(str);
						}
						printArray(recommArr);
					//	printArray(allPossible);
						
						/*let firstArray = getRecommendationCourses(9,"Think");
						let secondArray= getRecommendationCourses(9,"Communicate"); 
						let thirdArray=  getRecommendationCourses(9,"Grow"); 
						let fourthArray= getRecommendationCourses(9,"Solve"); */
						
						str= str.concat(firstArray,secondArray,thirdArray,fourthArray);
						//str.push();
						console.log("str",str);
						
						if(str.length>3){
							str= getAppropriateCourses(str);
						}
						console.log("improved str",str);	
						firstCourse= str[0];
						secondCourse= str[1];
						thirdCourse= str[2];
					}
				}
				i++;
				
                if (item.answers) {
                    if (item.answers[0].type == "number") {
                        
						if(i==0){
							sample= item.answers[0].field.type;
							//console.log("sdfsedda",item.answers[0].number);
							sampleData.push(item.answers[0]);
						}
                    }
                }
            })
		}
			/*
			To normalize 
				OLD PERCENT = (x - OLD MIN) / (OLD MAX - OLD MIN)
				NEW X = ((NEW MAX - NEW MIN) * OLD PERCENT) + NEW MIN
			*/
			function normalizeScore(score,oldRange){
				let oldPercent= score/oldRange;
				let newNo= 10*oldPercent;

				return Math.floor(newNo);
			}
			
			function getRecommendationCourses(score,habitName){
				var scoreType= getScoreType(score);
				var recommendedCourses=[];
				if(scoreType=="low"){
					//recommendedCourses= habitData[0].lowCourses;
					for(var i=0;i<habitData.length;i++){
						if(habitName==habitData[i].habitName){
							recommendedCourses= recommendedCourses.concat(habitData[i].lowCourses)
						}
					}
				}else if(scoreType=="medium"){
					//recommendedCourses.push(habitData[0].mediumCourses);
					for(var i=0;i<habitData.length;i++){
						if(habitName==habitData[i].habitName){
							recommendedCourses= recommendedCourses.concat(habitData[i].mediumCourses)
						}
					}
				}else{
					//recommendedCourses.push(habitData[0].highCourses);
					for(var i=0;i<habitData.length;i++){
						if(habitName==habitData[i].habitName){
							recommendedCourses= recommendedCourses.concat(habitData[i].highCourses)
						}
					}
				}
				console.log("recommendedCourses",recommendedCourses);
				return recommendedCourses;
				
			}
			
			function getAppropriateCourses(courses){
				let recommendedCourses= [];
				var firstCourseRank=20,secondCourseRank=20,thirdCourseRank=20;
				
				for(i=0;i<courses.length;i++){
					if(getCourseRanking(courses[i]) < firstCourseRank){
						thirdCourseRank= secondCourseRank;
						secondCourseRank= firstCourseRank;
						firstCourseRank= getCourseRanking(courses[i]);
					}else if (getCourseRanking(courses[i]) < secondCourseRank) { 
						thirdCourseRank = secondCourseRank; 
						secondCourseRank = getCourseRanking(courses[i]);
					}  else if (getCourseRanking(courses[i]) < thirdCourseRank){
						thirdCourseRank = getCourseRanking(courses[i]); 
					} 
				}
				recommendedCourses.push(getCourseName(firstCourseRank));
				recommendedCourses.push(getCourseName(secondCourseRank));
				recommendedCourses.push(getCourseName(thirdCourseRank));
				
				
				return recommendedCourses;
				
				
			}
			
			function getCourseName(courseRank){
				for(var i=0;i<coursesList.length;i++){
					if(courseRank == coursesList[i].rank){
						return coursesList[i].name;
					}
				}
			}
			
			function getCourseRanking(courseName){
				//console.log("coursdse",coursesList[0]);
				for(var i=0;i<coursesList.length;i++){
					if(courseName == coursesList[i].name){
						return coursesList[i].rank;
					}
				}
				
			}
			
			function getScoreType(score){
				var scoreType= "";
				//console.log("in scoreType",score);
				if(0 <= score && score<= 4){
					scoreType= "low";
				}else if(4 <score && score<= 7){
					scoreType=  "medium";
				}else if(7<score && score<=10){
					scoreType= "high";
				}
				return scoreType;
				
			}
			
			function cartesian() {
				var r = [], arg = arguments, max = arg.length-1;
				function helper(arr, i) {
					for (var j=0, l=arg[i].length; j<l; j++) {
						var a = arr.slice(0); // clone arr
						a.push(arg[i][j]);
						if (i==max)
							r.push(a);
						else
							helper(a, i+1);
					}
				}
				helper([], 0);
				return r;
			}
			function printArray(arr){
				for(var i=0;i<arr.length;i++){
					console.log("."+arr[i]);
				}
				
			}

            res.end(`
            <!DOCTYPE html>
            <html>
            <head>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
				<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
				<link type="text/css" href="styles.css">
				<style>
					
					.head{
					  font-family: segoe ui;
					  line-height: 1.4em;
					  font-size: 1.2em;
					  padding-left: 25%;
					  background: beige;
					  position: fixed;
					  width:100%;
					  margin-left: -1%;
					  margin-top: 1%;
					}
					
					
					.card{
						width: 18%;
						border: 1px solid gray;
						box-shadow: 4px 5px 7px #888;
						border-top: 10px solid green;
						min-height: 200px;
						padding: 10px;
						font-size: 20px;
						display: inline-block;
						background: linear-gradient(to bottom, #feffff 0%,#ddf1f9 35%,#a0d8ef 100%);
						margin-top: 10%;
					}
					.card span{
						margin-left:16%;
						display: block;
						margin-top: 30%;
					}
					.head span{
						margin-left: 24%;
						display: block;
						margin-top: 1%;
					}
					.first{
						margin-left:20%;
					}
					.second{
						margin-left:2%;
					}
					.third{
						margin-left:2%;
					}

				</style>
            </head>
            <body>
			

			<div class="head"><span>Your Recommended Courses</span></div>
			<div class="card first"><span>${firstCourse}</span></div>
			<div class="card second"><span>${secondCourse}</span></div>
			<div class="card third"><span>${thirdCourse}</span></div>
			
			
                <script>
                </script>
            </body>
            </html>
            `)
        })
        .catch(e => {
            console.log(e);
            return res.end(e.toString());
        })
    }
}

module.exports = Handlers;