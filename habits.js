const habits= 
		[
			{
				habitName: "Think",
				lowCourses: ["Thinking Critically","Reasoning Logically"],
				mediumCourses: ["Learning to learn","Decoding Others"],
				highCourses: ["Interpreting Self"]
			},
			{
				habitName:"Communicate",
				lowCourses: ["Writing Effectively","Speaking Impactfully"],
				mediumCourses: ["Listening Actively","Reading Deeply"],
				highCourses: ["Building Presence"]
			},
			{
				habitName:"Solve",
				lowCourses: ["Defining Problems","Structuring Problems"],
				mediumCourses: ["Planning Analyses","Creating Solutions"],
				highCourses: ["Taking Decisions"]
			},
			{
				habitName:"Grow",
				lowCourses: ["Managing Self","Practising Excellence"],
				mediumCourses: ["Leading Self","Embracing Change"],
				highCourses: ["Finding Happiness"]
			}
		]


module.exports= habits;