var courseList = [
				  {name:"Thinking Critically",rank:1},
				  {name:"Defining Problems",rank:2},
				  {name:"Writing Effectively",rank:3},
				  {name:"Managing Self",rank:4},
				  
				  
				  {name:"Reasoning Logically",rank:5},
				  {name:"Structuring Problems",rank:6},
				  {name:"Speaking Impactfully",rank:7},
				  {name:"Practising Excellence",rank:8},
				  
				  {name:"Learning to learn",rank:9},
				  {name:"Planning Analyses",rank:10},
				  {name:"Listening Actively",rank:11},
				  {name:"Leading Self",rank:12},
				  
				  {name:"Decoding Others",rank:13},
				  {name:"Creating Solutions",rank:14},
				  {name:"Reading Deeply",rank:15},
				  {name:"Embracing Change",rank:16},
				  
				  {name:"Interpreting Self",rank:17},
				  {name:"Taking Decisions",rank:18},
				  {name:"Building Presence",rank:19},				  
				  {name:"Finding Happiness",rank:20}
				]
				
module.exports= courseList;